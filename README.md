 Especificação projeto TFIN-Financeiro


	- Do que se trata? 
		
		Projeto TFIN, contempla cenários de abertura de contrato,reserva e gerenciamento de multas visando o modelo
		regressivo de testes. 
		


	- Onde queremos chegar !

		O mesmo tem como objetivo, realizar a cobertura regressiva de testes, sendo executado em um job no jenkins,
		porem por diversos impedimentos o projeto se encontra parado. 

	

	- Cobertura

		TFIN foi desenvolvido com 8 cenarios, cobrindo a abertura de contratos, reservas, contratos tripartites, mensal 
		flex e gerenciamento de multas, aplicando multas em contratos existentes.
		Todos os cenarios estao no modelo de Esquema do Cenário, onde podemos setar qualquer tipo de dado para teste, 
		possibilitanto a realização de cenarios negativos. 


	- Bugs 

		O time de desenvolvimento financeiro ART IT que fica em Campinhas-SP, possui um QA, e todas as tarefas de abertura e detecção de bugs sao direcionados
		a eles.
	

	- Problemas/Observações

		O objetivo de executar nossos cenarios em um job no jenkins visando a cobertura regressiva para subidas em produção 
		hoje se encontra com impedimento (CAPTCHA) no ambiente de homologação impedindo scripts automaticos.

		Foi combinado que todo bug aberto pela equipe da ART IT sera direcionado, para criarmos um script automatico
		para ser executados no modo regressivo para validaçoes. 					
	






